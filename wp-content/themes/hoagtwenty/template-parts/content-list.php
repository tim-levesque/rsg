<?php 
$title = get_the_title();
$link = get_the_permalink();
$job_options = get_post_meta( get_the_ID(), 'post_options', true);
//$job_options = get_post_meta( get_the_ID(), 'jobs_options' )[0];
 $salary = null;
if(isset($job_options['salary'])){
	$salary = $job_options['salary'];
}
?>
<div class="wp-block-group container-sm py-4 border-top">
    <div class="wp-block-group__inner-container">
        <div class="wp-block-group text-white"><div class="wp-block-group__inner-container">
            <h4><strong><?php echo $title;?></strong></h4>
            <p>Salary: <?php echo $salary;?></p>
        </div>
    </div>
    <div class="wp-block-buttons text-right d-block">
        <div class="wp-block-button text-primary">
            <a class="wp-block-button__link has-luminous-vivid-orange-color has-white-background-color has-text-color has-background no-border-radius" href="<?php echo $link;?>" rel=" float-right">Job Description &rarr;</a></div>
        </div>
    </div>
</div>



